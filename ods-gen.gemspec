# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

Gem::Specification.new do |s|
  s.name          = 'ods-gen'
  s.version       = '0.1.0'
  s.summary       = 'Generate an .ods file from a CSV string'
  s.description   = <<~DESC
    ods-gen can generate a semi-formatted .ods table from the command line,
    just by passing it a CSV.
  DESC
  s.authors       = ['Fabio Pesari']
  s.homepage      = 'https://gitlab.com/fpesari/ods-gen'
  s.files         = %w[.rubocop.yml ods-gen.gemspec LICENSE bin/ods-gen
                       README.md]
  s.executables   = %w[ods-gen]
  s.license       = 'AGPL-3.0+'
  s.add_runtime_dependency 'libxml-ruby', '~>3.2', '>=3.2.1'
  s.add_runtime_dependency 'rspreadsheet', '~>0.5', '>=0.5.3'
end
