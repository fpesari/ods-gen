# ods-gen - Generate an .ods spreadsheet from a CSV string

`ods-gen` can generate a semi-formatted .ods spreadsheet from the command line,
just by passing it a CSV string.

By default, it will generate a table which alternates white and grey rows,
for better readability.

## Installation

```
gem build ods-gen.gemspec
gem install ods-gen*.gemspec --user
```

## Usage

Open your terminal and run:

```
ods-gen NAME HEADERS DATA
```

`NAME` is the name of the spreadsheet, without `.ods`.
**Example**: `Elementary`

`HEADERS` is a comma-separated list of headers for the table.
**Example**: `A,B,C`

`DATA` is a colon-separated list of rows whose items are comma-separated.
**Example**: `1,2,3;4,5,6`

**Full example**: `ods-gen Elementary A,B,C 1,2,3;4,5,6`

`ods-gen` does not deal with existing files, it can only generate new
files from scratch.

## License

Everything in this repository is released under the
GNU Affero General Public License, version 3 or any later version, unless
otherwise specified (and even in that case, it's all free software).

